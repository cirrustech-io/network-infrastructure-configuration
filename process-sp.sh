#!/bin/bash
#
# Get new configurations from iTop and then run configuration backup on devices
#
# Ali Khalil
# 2014-08-07
#

function doit {
	# Pull device lists from iTop
	for devtype in routers switches firewalls voice managed
	do
		echo "Start pulling from itop for $devtype"
		/opt/gerty/infrastructure-sp/pull-from-itop.py $devtype 
		echo "End pulling from itop for $devtype"
	done

	# Run Gerty job
	echo "Start running gerty job"
	/opt/gerty/bin/gerty run --verbose /opt/gerty/infrastructure-sp/jobs/infrastructure-sp.ini 2>&1 | logger -t gerty -i
	echo "End running gerty job"

	# Add files to git staging, then commit before pushing to repository
	echo "Start performing git actions"
	cd /opt/gerty/infrastructure-sp
	git add data nodes
	git commit -m "`date`" 
	git remote | xargs -l git push
	echo "End performing git actions"
}

doit | logger -t gerty -i
