#!/usr/bin/python
# 
# Pull list of routers or switches from iTop using its export tool
# 
# Usage: python <SCRIPT> [routers|switches|voice|managed|firewalls]
# 
# Ali Khalil
# 2014-08-04
#

import requests, json
import sys
import StringIO
import csv


# Types of devices and their correspoing query ID in iTop
DEVICE_TYPES = {
	'routers':		3,
	'switches':		4,
	'voice':		5,
	'managed':		6,
	'firewalls':	7,
}

NODES_DIRECTORY = '/opt/gerty/infrastructure-sp/nodes/'

# Gerty credentials
USER_NAME = 'gerty'
PASS_WORD = 'tWxHtfeFad'

def fetch_csv_url( url ):
	'''
	Input URL which provides CSV Data
	Output _csv.reader object
	'''
	try:
		csv_data = requests.post(url, auth=(USER_NAME, PASS_WORD)).content
		# Remove headers and last line (blank or otherwise; WATCH OUT), then sort and remove duplicates
		raw_data = '\n'.join( sorted( set( csv_data.split('\n')[1:-1]) ) )
		return csv.reader( StringIO.StringIO(raw_data) )
	except Exception as e:
		print 'Problem with fetch_csv_url'
		print str(e)
		raise


def generate_from_itop(query):
	'''
	Retrieve CSV Data from iTop and process to produce Gerty compatible configuration
	'''
	try:
		# Get Devices information
		devices_info = fetch_csv_url("https://itop.2connectbahrain.com/webservices/export.php?format=csv&login_mode=basic&query="+query)
		
		# Get IP addresses information
		ips_info = fetch_csv_url("https://itop.2connectbahrain.com/webservices/export.php?format=csv&login_mode=basic&query=10&fields_advanced=1&fields=ip")

		# Create a dict for storing device information
		devices = {}
		ips = {}
		
		# Create dict for IP address with itop ID as key and address as value
		for row in ips_info:
			ips[ row[0] ] = row[1]

		for row in devices_info:
			# Easy naming for row fields
			brand = row[0]
			model = row[1]
			enclosure = row[2]
			ip_id = row[3]
			ip = ips[ip_id]
			
			if ip not in devices.keys():
				# Device title with device model
				devices[ip] = "%s--%s--%s"%(brand,model,ip,)
			else:
				# Device title with enclosure model
				devices[ip] = "%s--%s--%s"%(brand,enclosure,ip,)
		
		# Empty variable to hold configuration data which will be written to file
		config = ''
		
		# Spit out configuration in Gerty format
		for ip, name in devices.items():
			# Clean up unwanted characters
			name = name.replace(" ","_").replace("/","_")
			config = config + "%s // %s\n"%(name, ip)
		
		return config
	
	except Exception as e:
		print "Could not generate configuration from iTop."
		print str(e)
		raise
	
	
def write_gerty_config(query, data):
	'''
	Take string data and write to file
	'''
	filename = NODES_DIRECTORY + query
	try:
		f = open(filename,'w')
		f.write(data)
		f.close()
		print 'NODES configuration file created: %s'%filename
	except:
		print "Could not write to file."
		raise


if __name__ == '__main__':

	try:
		# Check for input parameter
		if len(sys.argv) > 1:
			query = sys.argv[1]
			if query in DEVICE_TYPES.keys():
				data = generate_from_itop( str(DEVICE_TYPES[query]) )
				write_gerty_config( query, data )
			else:
				# Invalid input parameter specified
				raise Exception('invalid_param')
		else:
			# No input parameter specified
			raise Exception('invalid_param')
	except Exception as e:
		if e.args[0] == 'invalid_param':
			print "Usage: python <THIS_SCRIPT> [routers|switches|voice|managed|firewalls]"
		else:
			print 'Error completing run with query: %s'%(query)
	
